package ge.beka.homework3

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import ge.beka.homework3.models.ItemModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private val addItemCode = 201
    public val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        addItem.setOnClickListener {
            openItemAdderActivity()
        }

        recyclerViewMain.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        recyclerViewMain.adapter = adapter

        setExample()
    }

    private fun openItemAdderActivity(){
        val intent = Intent(this, AddItemActivity::class.java)
        startActivityForResult(intent, addItemCode)
    }

    private fun addItem(itemData: ItemModel){
        items.add(itemData)
        adapter.notifyItemInserted(items.size - 1)
        recyclerViewMain.scrollToPosition(items.size - 1)
    }

    private fun setExample(){
        items.add(ItemModel(R.mipmap.avatar, "Test Title", "Test Description", Date().toString()))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == addItemCode && resultCode == Activity.RESULT_OK){
            val itemData = data!!.getParcelableExtra("itemData") as ItemModel
            addItem(itemData)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


}
