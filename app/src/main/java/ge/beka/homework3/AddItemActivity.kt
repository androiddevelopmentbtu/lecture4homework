package ge.beka.homework3

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ge.beka.homework3.models.ItemModel
import kotlinx.android.synthetic.main.activity_add_item.*
import java.util.*

class AddItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
        init()
    }

    private fun init(){
        addItemButton.setOnClickListener{
            saveItem()
        }
    }

    private fun saveItem(){
        val itemModel = ItemModel(
            R.mipmap.avatar,
            titleEditText.text.toString(),
            descriptionEditText.text.toString(),
            Date().toString()
        )

        val intent = intent
        intent.putExtra("itemData", itemModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
